<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacationHistory extends Model
{
    use HasFactory;
    protected $table = 'MBS_VACATION_HISTORY';
}
