<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartialLeaves extends Model
{
    use HasFactory;
    protected $table = 'MBS_PARTIAL_LEAVES';
}
