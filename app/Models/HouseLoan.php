<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HouseLoan extends Model
{
    use HasFactory;
    protected $table = 'MBS_HOUSE_LONE_PAYMENT';
}
