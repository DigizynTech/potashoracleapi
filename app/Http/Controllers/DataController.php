<?php

namespace App\Http\Controllers;

use App\Models\EmployeeAttendance;
use App\Models\EmployeeInfo;
use App\Models\HouseLoan;
use App\Models\MedicalSlip;
use App\Models\PartialLeaves;
use App\Models\PaySlip;
use App\Models\VacationBalance;
use App\Models\VacationHistory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

class DataController extends Controller
{
    public function getEmployeeInfo(Request $request): JsonResponse
    {

        $employee = EmployeeInfo::where('employee_number', $request->input('employee_id'))->first();
        if ($employee) {
            $message = [
                'status' => 200,
                'response' => $employee
            ];
        } else {
            $message = [
                'status' => 201,
                'response' => 'Employee #' . $request->input('employee_id') . ' Not found '
            ];
        }

        return Response::json($message);
    }

    public function getEmployeeAttendance(Request $request): JsonResponse
    {
        $data = EmployeeAttendance::where('employee_number', $request->input('employee_id'))
            ->orderByDesc('da_date')
            ->get();

        if ($data) {
            $message = [
                'status' => 200,
                'response' => ['attendance' => $data]
            ];
        } else {
            $message = [
                'status' => 201,
                'response' => 'Employee #' . $request->input('employee_id') . ' Not found '
            ];
        }
        return Response::json($message);
    }

    public function getPartialLeaves(Request $request): JsonResponse
    {
        $data = PartialLeaves::where('employee_number', $request->input('employee_id'))->get();

        if ($data) {
            $message = [
                'status' => 200,
                'response' => ['attendance' => $data]
            ];
        } else {
            $message = [
                'status' => 201,
                'response' => 'Employee #' . $request->input('employee_id') . ' Not found '
            ];
        }
        return Response::json($message);
    }

    public function getPaySlip(Request $request): JsonResponse
    {
        $data = PaySlip::where('employee_number', $request->input('employee_id'))->get();

        if ($data) {
            $message = [
                'status' => 200,
                'response' => ['attendance' => $data]
            ];
        } else {
            $message = [
                'status' => 201,
                'response' => 'Employee #' . $request->input('employee_id') . ' Not found '
            ];
        }
        return Response::json($message);
    }

    public function getVacationBalance(Request $request): JsonResponse
    {
        $data = VacationBalance::where('employee_number', $request->input('employee_id'))->first();

        if ($data) {
            $message = [
                'status' => 200,
                'response' => $data
            ];
        } else {
            $message = [
                'status' => 201,
                'response' => 'Employee #' . $request->input('employee_id') . ' Not found '
            ];
        }
        return Response::json($message);
    }

    public function getVacationHistory(Request $request): JsonResponse
    {
        $data = VacationHistory::where('employee_number', $request->input('employee_id'))
            ->orderByDesc('abs_date_from')
            ->get();

        if ($data) {
            $message = [
                'status' => 200,
                'response' => ['vacations' => $data]
            ];
        } else {
            $message = [
                'status' => 201,
                'response' => 'Employee #' . $request->input('employee_id') . ' Not found '
            ];
        }
        return Response::json($message);
    }

    public function getLeavesHistory(Request $request): JsonResponse
    {
        $data = PartialLeaves::where('employee_number', $request->input('employee_id'))
            ->orderByDesc('date_from')
            ->get();

        if ($data) {
            $message = [
                'status' => 200,
                'response' => ['leaves' => $data]
            ];
        } else {
            $message = [
                'status' => 201,
                'response' => 'Employee #' . $request->input('employee_id') . ' Not found '
            ];
        }
        return Response::json($message);
    }

    public function getMedicalInfo(Request $request): JsonResponse
    {
        $MedicalCuts = MedicalSlip::where('employee_number', $request->input('employee_id'))->get();
//        $totalDeductions = $MedicalCuts->sum(function ($cut) {
//            return (float)$cut['ded_tot'];
//        });

        if ($MedicalCuts->count() == 0) {
            $message = [
                'status' => 200,
                'response' => [
                    'MedicalDeductions' => null,
                    'BalanceAfterDeduction' => null,
                    'Month' => null,
                    'LatestVisits' => null,
                    'TotalAmount' => null,
                    'TotalDeduction' => null,
                    'MedicalVisits' => null
                ]
            ];

            return Response::json($message);
        }

        $totalDeductions = $MedicalCuts->first()->act_deduction;
        $BalanceAfterDeduction = $MedicalCuts->first()->balance_after_ded;
        $LatestVisits = [];
        $TotalAmount = 0;
        $TotalDeduction = 0;
        $ActualDedMonth = 11;
        foreach ($MedicalCuts as $medicalCut) {
            $LatestVisits[] = [
                'visit_date' => $medicalCut->patient_visit_date,
                'nath_v_name' => $medicalCut->nath_v_name,
                'patient' => $medicalCut->patient,
                'amount' => $medicalCut->amount,
                'deducted' => $medicalCut->ded_tot,
                'actual_ded_month' => $medicalCut->actual_ded_month,
            ];
            $TotalAmount += (float)$medicalCut->amount;
            $TotalDeduction += (float)$medicalCut->ded_tot;
            $ActualDedMonth = $medicalCut->actual_ded_month;
        }

        $message = [
            'status' => 200,
            'response' => [
                'MedicalDeductions' => $totalDeductions,
                'BalanceAfterDeduction' => $BalanceAfterDeduction,
                'Month' => $ActualDedMonth,
                'LatestVisits' => $LatestVisits,
                'TotalAmount' => $TotalAmount,
                'TotalDeduction' => $TotalDeduction,
                'MedicalVisits' => $MedicalCuts->count()
            ]
        ];

        return Response::json($message);
    }

    public function getAllDepartments(): JsonResponse
    {
        $employees = EmployeeInfo::all();
        $departments = $employees->pluck('organization_name')->unique();

        $message = [
            'status' => 200,
            'response' => [
                'departments' => $departments
            ]
        ];

        return Response::json($message);
    }

    public function getHouseLoans(Request $request): JsonResponse
    {
        $HouseLoan = HouseLoan::where('employee_number', $request->input('employee_id'))->first();

        if ($HouseLoan) {
            $message = [
                'status' => 200,
                'response' => [
                    'opening_balance' => $HouseLoan->opening_balance,
                    'installment_payment' => $HouseLoan->installment_payment,
                    'remaining_balance' => $HouseLoan->remaining_balance
                ]
            ];
        } else {
            $message = [
                'status' => 200,
                'response' => [
                    'opening_balance' => 0,
                    'installment_payment' => 0,
                    'remaining_balance' => 0
                ]
            ];
        }

        return Response::json($message);
    }
}
