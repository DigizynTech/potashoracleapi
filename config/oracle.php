<?php


return [
    'oracle' => [
        'driver' => 'oracle',
        'host' => env('DB_HOST','10.20.164.18'),
        'port' => env('DB_PORT', '1521'),
        'database' => env('DB_DATABASE','default'),
        'service_name' => env('DB_SERVICE_NAME','default'),
        'username' => env('DB_USERNAME','username'),
        'password' => env('DB_PASSWORD', 'password'),
        'charset' => '',
        'prefix' => '',
    ],
];
