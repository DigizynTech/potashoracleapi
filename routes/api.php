<?php

use App\Http\Controllers\DataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware(['specialToken'])->group(function () {
    Route::get('EmployeeInfo', [DataController::class, 'getEmployeeInfo']);
    Route::get('EmployeeAttendance', [DataController::class, 'getEmployeeAttendance']);
    Route::get('PartialLeaves', [DataController::class, 'getPartialLeaves']);
    Route::get('PaySlip', [DataController::class, 'getPaySlip']);
    Route::get('VacationBalance', [DataController::class, 'getVacationBalance']);
    Route::get('VacationHistory', [DataController::class, 'getVacationHistory']);
    Route::get('LeavesHistory', [DataController::class, 'getLeavesHistory']);
    Route::get('MedicalInfo', [DataController::class,'getMedicalInfo']);
    Route::get('HouseLoan', [DataController::class,'getHouseLoans']);
    Route::get('GetAllDepartments', [DataController::class,'getAllDepartments']);
});
